package blackswan.cucumber.CM.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


/**
 * Represents element like textfield
 * @author rashad.r@vendorpanel.com
 */

public class TextField  extends Element {

	
    public TextField(WebDriver driver, By elementIdentifier) {
        super(driver, elementIdentifier);
    }
    
    /**
  	 * Enter the text in testField
  	 * 
  	 */
    
    public void enterText(String text){
        getWebElement().sendKeys(text);
    }
    
    /**
  	 * clear the text in textfield
  	 * 
  	 */
    
    public void clearText(){
        getWebElement().clear();
    }
    
    /**
  	 * get text from testField
  	 * 
  	 */
    public String getText(){
        return getElementAttribute("value");
    }
}
