package blackswan.cucumber.CM.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePageCM extends BasePageFactory{

	/**
	 * 
	 * Define WebDriver
	 */
	 WebDriver driver;

	public HomePageCM(WebDriver driver) {
		
		super(driver);
	}

	@FindBy(how = How.ID, using = "q")
	public static WebElement txtSearchField;
	
	@FindBy(how = How.XPATH, using = "//div[@class='search-box__search--2fC5']/descendant::button")
	public static WebElement btnSearch;
	
	@FindBy(how = How.ID, using = "anonLogin")
	public static WebElement linkLogin;
	
	@FindBy(how = How.XPATH, using = "//div[@id='topActionUserAccont']/descendant::span[@id='myAccountTrigger']")
	public static WebElement userAccount;

	
	public void enterSearchValue(String username) {
		txtSearchField.sendKeys(username);
	}
	
	public void clickSearch() {
		btnSearch.click();
	}
	
	public void loginLink() {
		linkLogin.click();
	}
	
	public String userAccount_Text() {
		
		 return userAccount.getText();
	}

	
	
	
	
}
