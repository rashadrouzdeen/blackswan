package blackswan.cucumber.CM.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ProductsPage extends BasePageFactory {

	/**
	 * 
	 * Define WebDriver
	 */
	WebDriver driver;

	public ProductsPage(WebDriver driver) {

		super(driver);
	}

	@FindBy(how = How.XPATH, using = "//div[@class='c1_t2i']/descendant::div[@class='cRjKsc'][1]")
	public static WebElement linkFirstProduct;
	
	@FindBy(how = How.XPATH, using = "//div[@class='pdp-block pdp-block__product-ads']/descendant::h6[1]")
	public static WebElement lblSameStore;


	public void clickFirstProduct() {
		linkFirstProduct.click();
	}
	
	public String SameStore() {
		 return lblSameStore.getText();
	}

}