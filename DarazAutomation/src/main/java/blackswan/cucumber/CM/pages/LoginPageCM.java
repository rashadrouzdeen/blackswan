package blackswan.cucumber.CM.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPageCM extends BasePageFactory{

	/**
	 * 
	 * Define WebDriver
	 */
	 WebDriver driver;

	public LoginPageCM(WebDriver driver) {
	
		super(driver);
		
	}

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Please enter your Phone Number or Email']")
	static WebElement txtbx_UserName;

	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	static WebElement txtbx_password;

	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	static WebElement btn_Next;

	
	public void typeUserName(String username) {
		txtbx_UserName.sendKeys(username);
	}
	
	public void txtbx_password(String pword) {
		txtbx_password.sendKeys(pword);
	}
	
	public void btn_Next() {
		btn_Next.click();
	}

	
}
