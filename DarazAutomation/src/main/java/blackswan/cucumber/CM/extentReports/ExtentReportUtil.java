package blackswan.cucumber.CM.extentReports;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReportUtil extends BaseUtil {

	String  fileName = reportLocation + "extentreport.html";
	
	public void ExtentReport() {
		
		extent = new ExtentReports();
		
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setDocumentTitle("Contracts Monitor");
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setReportName("Test Report");
		
		extent.attachReporter(htmlReporter);
		
	}
	
	/*public void ExtentReportScreenshot(Scenario scenario, String screenShotFile) {
		
		if(scenario!=null) {
			
			switch(scenario.getStatus()) {
			
			
		}
			
		}
		
		//var scr = ((TakesScreenshot)Driver).getScreenshotAs(OutputType.FILE);
		
		
	}*/
	
	public void FlushReport() {
		
		extent.flush();
		
	}
	
	
}
