package blackswan.cucumber.CM.extentReports;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

public class BaseUtil {

	public WebDriver Driver;
	
	public ExtentReports extent;
	
	public static ExtentTest scenarioDef;
	
	public static ExtentTest feature;
	
	public static String reportLocation = "Users/HP/Documents/JavaCucumber/Reports/";
	
	
}
