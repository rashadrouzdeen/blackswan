package blackswan.cucumber.CM.core.info;

import java.io.InputStream;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;




public class DataProvider {

	   /**

     * Get Test data by key

     *

      * @param key

      * @return Test data for given key

     */

	 String path = System.getProperty("C:\\Users\\HP\\Documents\\JavaCucumber\\vendorpanel.cucumber.CM\\src\\main\\resources");


     public static String  getDataByKey(String key)

     {

                    Yaml yaml = new Yaml();
                    InputStream inputStream = DataProvider.class.getClassLoader().getResourceAsStream("data.yml");
                    Map<String, Object> obj = yaml.load(inputStream);
                    String value=(String) obj.get(key);
                    return value;

     }
	
	
}
