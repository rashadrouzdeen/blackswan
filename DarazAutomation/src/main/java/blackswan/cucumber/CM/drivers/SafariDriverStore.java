package blackswan.cucumber.CM.drivers;

import org.openqa.selenium.safari.SafariDriver;

/**
 * @author rashad.r@vendorpanel.com
 *
 */

public class SafariDriverStore {

	/**
	 * Defining WebDriver
	 */
	private SafariDriver driver;

	/**
	 * @return
	 */
	public SafariDriver createWebDriver() {
		driver = new SafariDriver();
		return driver;
	}
	
	
}
