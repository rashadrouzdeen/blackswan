package blackswan.cucumber.CM.drivers;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseClass {

	public static WebDriver driver;
	
	public  void  setup(String url) {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\Documents\\seleniumwebdriver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1560, 960));
		driver.get(url);
		
	}
	
}
