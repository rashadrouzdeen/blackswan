package blackswan.cucumber.CM.drivers;

import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * @author rashad.r@vendorpanel.com
 *
 */

public class InternetExplorerDriverStore {

	/**
	 * Defining WebDriver
	 */
	private InternetExplorerDriver driver;
	/**
	 * Defining driverPath
	 */
	private String driverPath;

	/**
	 * @param driverPath
	 */
	public InternetExplorerDriverStore(String driverPath) {
		this.driverPath = driverPath;
		System.setProperty("webdriver.ie.driver", this.driverPath);
	}

	/**
	 * @return
	 */
	public InternetExplorerDriver createWebDriver() {
		
		this.driver = new InternetExplorerDriver();
		return driver;
	}
	
	
	
	
}
