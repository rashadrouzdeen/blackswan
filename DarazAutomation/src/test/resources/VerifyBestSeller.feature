Feature: Verify Best Seller Component
  I want to enter the site and verify the visibility of the best seller component

  @tag1
  Scenario: Best Seller Component
    Given I want to navigate to the site
    And I enter the product on search bar
    When I click the search button
    And I click the first item on the product list
    Then I validate Best Seller Component


  