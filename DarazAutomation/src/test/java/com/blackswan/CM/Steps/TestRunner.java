package com.blackswan.CM.Steps;

import org.junit.runner.RunWith;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features ="classpath:/src/test/resources", 
		glue="Steps",
		tags="",
		plugin = {"pretty",
				"html:target/html/",
				"jsoin:target/json/file.json",
				"com.aventstack.extentReports.cucumber.adapter.ExtentCucumber.Adapter:"
				}, 
				strict=false,
				dryRun=false
		
		)



public class TestRunner {
	
	

}
