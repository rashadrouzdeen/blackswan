package com.blackswan.CM.Steps;

import static org.testng.Assert.assertEquals;

import blackswan.cucumber.CM.core.info.DataProvider;
import blackswan.cucumber.CM.drivers.BaseClass;
import blackswan.cucumber.CM.pages.HomePageCM;
import blackswan.cucumber.CM.pages.ProductsPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class VerifyBestSeller_StepDefinition extends BaseClass {

	
	 HomePageCM homePageCM;
	 ProductsPage productsPage;
	
	@Given("I want to navigate to the site")
	public void i_want_to_navigate_to_the_site() {
		
		super.setup(DataProvider.getDataByKey("url"));
	}

	@Given("I enter the product on search bar")
	public void i_enter_the_product_on_search_bar() {
	    
		homePageCM = new HomePageCM(driver);
		homePageCM.enterSearchValue(DataProvider.getDataByKey("product"));
		
	}

	@When("I click the search button")
	public void i_click_the_search_button() {
		homePageCM = new HomePageCM(driver);
		homePageCM.clickSearch();
	}

	@When("I click the first item on the product list")
	public void i_click_the_first_item_on_the_product_list() throws InterruptedException {
		productsPage = new ProductsPage(driver);
		productsPage.clickFirstProduct();
		Thread.sleep(3000);
	}

	@Then("I validate Best Seller Component")
	public void i_validate_Best_Seller_Component() {
		productsPage = new ProductsPage(driver);
		//Assumed that "From The Same Store" is Best seller component
		assertEquals("From The Same Store", productsPage.SameStore());
		driver.quit();
	}
	
	
	
}
