package com.blackswan.CM.Steps;

import static org.testng.Assert.assertEquals;

import blackswan.cucumber.CM.core.info.DataProvider;
import blackswan.cucumber.CM.drivers.BaseClass;
import blackswan.cucumber.CM.pages.HomePageCM;
import blackswan.cucumber.CM.pages.LoginPageCM;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Login_Daraz_StepDefinition extends BaseClass{

		LoginPageCM loginpagecm;
		HomePageCM homePageCM;

	
	
	@Given("I am on the homepage page")
	public void i_am_on_the_homepage_page() {
		
		super.setup(DataProvider.getDataByKey("url"));

	    
	}

	@Given("I press Login on homepage")
	public void i_press_Login_on_homepage() {
		homePageCM = new HomePageCM(driver);
		homePageCM.loginLink();

	}

	@When("I fill in Email with")
	public void i_fill_in_Email_with() {
	    
		loginpagecm = new LoginPageCM(driver);
		loginpagecm.typeUserName(DataProvider.getDataByKey("user_email"));

	}

	@When("I fill in Password with")
	public void i_fill_in_Password_with() {
	  
		loginpagecm.txtbx_password(DataProvider.getDataByKey("password"));

	}

	@When("I press Login")
	public void i_press_Login() {
	    
		loginpagecm.btn_Next();

	}

	@Then("I should be on the users home page")
	public void i_should_be_on_the_users_home_page() throws InterruptedException {
	    Thread.sleep(3000);
		System.out.println("Test======================================"+homePageCM.userAccount_Text()+"=====================");
		assertEquals("BLACKSWAN'S ACCOUNT", homePageCM.userAccount_Text());
		driver.quit();

		
	}

	
	
}
